# Cave generation

Generate random cave-like structures using cellular automata.
Inspired by this [blog post](https://jeremykun.com/2012/07/29/the-cellular-automaton-method-for-cave-generation/).