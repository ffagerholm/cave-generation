from copy import deepcopy
import numpy as np


class CellularAutomaton():
    def __init__(self, grid, rules):
        """
        Cellular automaton
        Create a cellular automaton on a 2D grid.
        Args:
            grid (numpy.array): (n,m) dimensional numpy array
            rules (dict[str] = list(int)): update rules of the automaton.
                Has to have the keys 'B' and 'S'. 
                The key 'B' maps to the
                list of integers that specify for which number of alive
                neighbors a new cell should be born. 
                The key 'S' maps to the
                list of integers that specify for which number of alive
                neighbors a cell should stay alive (survive).
        """
        self.grid = deepcopy(grid)
        self.rules = deepcopy(rules)
        self.n_rows = grid.shape[0]
        self.n_cols = grid.shape[1]

    def _alive_neighbors(self, x, y):
        """Get number of alive neighbors.
        Get the number of alive cells on the grid that have
        distance 1 to the cell (x, y).
        Args:
            x (int): x-coordinate of cell.
            y (int): y-coordinate of cell.
            
        """
        n_alive = 0
        for i in [-1, 0, 1]:
            for j in [-1, 0, 1]:
                # check that we don't go outside the grid
                # and don't include the point (x, y)
                if 0 <= x + i < self.n_rows and \
                    0 <= y + j < self.n_cols and \
                    (i != 0 or j != 0):
                    n_alive += self.grid[x + i, y + j]

        return n_alive

    def update(self):
        """Update grid according to the rules of the automaton.
        """
        # TODO: make update more efficient 
        # (can dynamic programming be used?)
        updated_grid = self.grid.copy()

        for i in range(self.n_rows):
            for j in range(self.n_cols):
                n_alive = self._alive_neighbors(i, j)
                if self.grid[i, j] == 0 and (n_alive in self.rules['B']):
                    updated_grid[i, j] = 1
                elif self.grid[i, j] == 1 and (n_alive in self.rules['S']):
                    updated_grid[i, j] = 1
                else:
                    updated_grid[i, j] = 0
        
        self.grid = updated_grid

    def print_neighbourhood(self):
        """
        """
        updated_grid = self.grid.copy()

        for y in range(1, self.n_rows - 1):
            environment = self.grid[y - 1, 0] + self.grid[y - 1, 1] + \
                          self.grid[y,     0] + \
                          self.grid[y + 1, 0] + self.grid[y + 1, 1]
            for x in range(1, self.n_cols - 1):
                environment += self.grid[y - 1, x + 1] + \
                               self.grid[y    , x + 1] + \
                               self.grid[y + 1, x + 1]
                print((x, y), environment)
                    
    
    def print_alive(self):
        for i in range(1, self.n_rows - 1):
            for j in range(1, self.n_cols - 1):
                n_alive = self._alive_neighbors(i, j)
                print((i, j), n_alive)
        
                     