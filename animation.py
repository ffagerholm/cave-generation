import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation

from cellular_automaton import CellularAutomaton


def main():
    """
    Conway's Game of Life on a randomly initialized board
    """
    grid = np.random.choice([0, 1], size=(50, 50))
    # create a "Game of Life" (B3/S23) automaton
    # initialized with the random grid
    automaton = CellularAutomaton(grid, 
                                  rules={'B': [3, 4],
                                         'S': [2, 3]})
    # init graphics
    fig, ax = plt.subplots()
    game_board = plt.imshow(grid, cmap=plt.cm.Greys, animated=True)
    
    def update(frame):
        automaton.update()
        game_board.set_data(automaton.grid)
        return game_board,

    ani = FuncAnimation(fig, update, frames=range(1000), blit=True)
    plt.show()


if __name__ == '__main__':
    main()
