import numpy as np
from scipy import ndimage
import matplotlib.pyplot as plt

from cellular_automaton import CellularAutomaton


def generate_cave(size=(100, 100), prop=0.5):
    """
    Generate a random cave.

    Args:
        size (tuple): size of the grid
        prop (float): proportion of zeroes in the grid
    """
    n_rows, n_cols = size
    # initialize grid with random values
    grid = np.random.choice([0, 1], size=size, p=[prop, 1.0 - prop])
    # fill border
    grid[:,  0] = 1
    grid[:, -1] = 1
    grid[ 0, :] = 1
    grid[-1, :] = 1

    # create B678/S345678 automaton
    # initialized with the random grid
    automaton = CellularAutomaton(grid, 
                    rules={'B': [6, 7, 8],
                           'S': [3, 4, 5, 6, 7, 8]})
    
    # run automaton for 20 steps,
    # it should converge fast
    for _ in range(15):
        automaton.update() # update grid
    
    # expand grid by a factor 4
    large_grid = ndimage.zoom(automaton.grid, 4, order=0)
    
    smoothing_automaton = CellularAutomaton(large_grid, 
                                    rules={'B': [5, 6, 7, 8],
                                           'S': [5, 6, 7, 8]})    
    # run smoothing
    for _ in range(3):
        smoothing_automaton.update() # update grid

    return smoothing_automaton.grid


if __name__ == '__main__':
    cave = generate_cave(size=(50, 50), prop=0.55)
    plt.imshow(cave, cmap=plt.cm.Greys)
    plt.show()